const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

 mix.js('resources/js/app.js', 'public/js')
	.js('resources/js/parralax.js', 'public/js')
	.js('resources/js/realisations.js', 'public/js')
	.js('resources/js/captcha.js', 'public/js')
	.js('resources/js/ajax.js', 'public/js')
	.copy('resources/js/lazy-load.js', 'public/js')
    .sass('resources/scss/app.scss', 'public/css')
    .sass('resources/scss/index.scss', 'public/css')
    .sass('resources/scss/realisations.scss', 'public/css')
    .sass('resources/scss/taillemaconnerie.scss', 'public/css')
    .sass('resources/scss/contact.scss', 'public/css')
    .sass('resources/scss/resp/index.scss', 'public/css')
    .sass('resources/scss/resp/realisations.scss', 'public/css')
    .sass('resources/scss/resp/taillemaconnerie.scss', 'public/css')
    .sass('resources/scss/resp/all.scss', 'public/css')
    .sourceMaps();

	if(mix.inProduction()){
		mix.version();
	}
