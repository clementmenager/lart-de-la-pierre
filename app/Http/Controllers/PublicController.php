<?php
namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Realisation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;

class PublicController extends Controller {
	public function index(){
		if (session()->has('locale')) App::setlocale(session()->get('locale'));
		$realisations = Realisation::get();
		$avis=0;
		foreach ($realisations as $realisation) {
			if($realisation->avis_titre) $avis++;
		}
		return view('pages.default.index', compact('realisations', 'avis'));
	}

	public function realisations(){
		$categories = Category::get();
		$realisations = Realisation::orderBy('id', 'desc')->get();
		return view("pages.default.realisations", compact('realisations','categories'));
	}

	public function lang(){
		$locale=(App::currentLocale()=='fr') ? "en" : "fr";
		App::setLocale($locale);
		session()->put('locale', $locale);

		return redirect()->back();
	}

	public function sendmail(Request $request){
		function getCaptcha($publicresponse){
			$response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".env('CAPTCHA_SECRET')."&response={$publicresponse}");
			return json_decode($response);
		}
		$return = getCaptcha($request->{'g-recaptcha-response'});
		if($return->success == true && $return->score > 0.5){

			$validator = Validator::make($request->all(), [
				'nom' => 'required',
				'prenom' => 'required',
				'email' => 'required|email',
				'tel' => 'required',
			]);
			if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();
			$destinataire = "contact@artdelapierre.net";
			$sujet = "Formulaire de contact LaToile.dev";
			$contenu= "Envoyé par : $request->nom $request->prenom<br>Téléphone: $request->tel<br>Message:<br>$request->message";
			$mailheader = array(
				'From' => $destinataire,
				'CC' => $request->email,
				'X-Mailer' => 'PHP/' . phpversion(),
				'X-Priority' => '1',
				'Mime-Version' => '1.0',
				'Content-type' => 'text/html; charset= utf-8',
				'charset' => 'UTF-8'
			);
			mail($destinataire, $sujet, $contenu, $mailheader) or die("Erreur!");
			return redirect(route('contact'))->with('success', "Votre message a bien été envoyé, nous vous répondrons au plus vite.");
		}else{
			return redirect(route('contact'))->with('error', "Vous avez été considéré comme un robot merci de bien vouloir réessayez.");
		}
	}
}
