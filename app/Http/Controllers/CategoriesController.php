<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

class CategoriesController extends Controller{
	/**
	* Display a listing of the resource.
	*
	* @return \Illuminate\Http\Response
	*/
	public function index(){
		$collection = Category::get();
		return view('pages.admin.list',compact('collection'));
	}

	/**
	* Show the form for creating a new resource.
	*
	* @return \Illuminate\Http\Response
	*/
	public function create(){
		$item=New Category;
		return view('pages.admin.create',compact('item'));
	}

	/**
	* Store a newly created resource in storage.
	*
	* @param  \Illuminate\Http\Request  $request
	* @return \Illuminate\Http\Response
	*/
	public function store(Request $request){
		$request->validate(['titre' => 'required']);
		Category::create($request->all());
		return redirect()->route('categories.index')->with('success','Create Successfully');
	}

	/**
	* Show the form for editing the specified resource.
	*
	* @param  int  $id
	* @return \Illuminate\Http\Response
	*/
	public function edit($id){
		$item = Category::find($id);
		return view('pages.admin.edit',compact('item'));
	}

	/**
	* Update the specified resource in storage.
	*
	* @param  \Illuminate\Http\Request  $request
	* @param  int  $id
	* @return \Illuminate\Http\Response
	*/
	public function update(Request $request, $id){
		$category = Category::findOrFail($id);
		$request->validate(['titre' => 'required']);
		$category->update($request->all());
		return redirect()->route('categories.index')->with('success','Update Successfully');
	}

	/**
	* Remove the specified resource from storage.
	*
	* @param  int  $id
	* @return \Illuminate\Http\Response
	*/
	public function destroy($id){
		Category::where('id',$id)->delete();
		return redirect()->route('categories.index')->with('success','Delete Successfully');
	}
}
