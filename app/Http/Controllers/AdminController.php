<?php
namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller {
	public function mdp(){
		$user = User::get();
		$mdp = User::get()[0]->password;
		return view('pages.admin.user',compact('user','mdp'));
	}

	public function recup(Request $request){
		$ancien=$request->ancien;
		$mdp = User::find(2)->password;
		if (Hash::check($ancien, $mdp)) {
			if($request->nouveau == $request->confirmation){

				// $request->validation(['nouveau'=>'required']);
				User::find(2)->update([
					'password' => bcrypt($request->nouveau)
				]);
				return redirect(route('user'))->with('success', "Mot de passe modifier.");
			}else{
				return redirect(route('user'))->with('error', "Problème de confirmation du nouveau mot de passe");
			}
		}else {
			return redirect(route('user'))->with('error', "Ancien mot de passe saisie Faux");
		}
	}

}
