<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Models\Realisation;
use App\Models\Category;

use File;
use Image;

class RealisationController extends Controller{
	/**
	* Display a listing of the resource.
	*
	* @return \Illuminate\Http\Response
	*/
	public function index(){
		$collection = Realisation::orderBy('id', 'DESC')->get();
		return view('pages.admin.list',compact('collection'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create(){
		$realisation=New Realisation;
		$categories=Category::get()->pluck("titre","id")->toArray();
		return view('pages.admin.create',compact('realisation','categories'));
	}

	/**
	* Store a newly created resource in storage.
	*
	* @param  \Illuminate\Http\Request  $request
	* @return \Illuminate\Http\Response
	*/
	public function store(Request $request){
		$request->validate(['titre' => 'required']);
		$realisation=New Realisation;
		$avant = $this->image($realisation,$request,'avant');
		$apres = $this->image($realisation,$request,'apres');

		Realisation::create([
			'titre'=>$request->titre,
			'slug'=>$request->slug,
			'description'=>$request->description,
			'avant'=>$avant,
			'apres'=>$apres,
			'category_id'=>$request->category_id,
			'avis_titre'=>$request->avis_titre,
			'avis_description'=>$request->avis_description
		]);
		return redirect()->route('realisations.index')->with('success','Create Successfully');
	}

	/**
	* Show the form for editing the specified resource.
	*
	* @param  int  $id
	* @return \Illuminate\Http\Response
	*/
	public function edit($id){
		$realisation = Realisation::findOrFail($id);
		$categories=Category::get()->pluck("titre","id")->toArray();
		return view('pages.admin.edit',compact('realisation','categories'));
	}

	/**
	* Update the specified resource in storage.
	*
	* @param  \Illuminate\Http\Request  $request
	* @param  int  $id
	* @return \Illuminate\Http\Response
	*/
	public function update(Request $request, $id){
		$request->validate(['titre' => 'required']);

		$realisation = Realisation::findOrFail($id);
		$avant = $this->image($realisation,$request,'avant');
		$apres = $this->image($realisation,$request,'apres');

		$realisation->update([
			'titre'=>$request->titre,
			'slug'=>$request->slug,
			'description'=>$request->description,
			'avant'=>$avant,
			'apres'=>$apres,
			'category_id'=>$request->category_id,
			'avis_titre'=>$request->avis_titre,
			'avis_description'=>$request->avis_description
		]);
		return redirect()->route('realisations.index')->with('success','Update Successfully');
	}

	/**
	* Remove the specified resource from storage.
	*
	* @param  int  $id
	* @return \Illuminate\Http\Response
	*/
	public function destroy($id){
		$realisation = Realisation::findOrFail($id);
		if($realisation->avant) {
			File::delete(public_path("/images/reals/default/".$realisation->avant));
			File::delete(public_path("/images/reals/2000/".$realisation->avant));
			File::delete(public_path("/images/reals/1500/".$realisation->avant));
			File::delete(public_path("/images/reals/800/".$realisation->avant));
		};
		if($realisation->apres) {
			File::delete(public_path("/images/reals/default/".$realisation->apres));
			File::delete(public_path("/images/reals/2000/".$realisation->apres));
			File::delete(public_path("/images/reals/1500/".$realisation->apres));
			File::delete(public_path("/images/reals/800/".$realisation->apres));
		};
		$realisation->delete();
		return redirect()->route('realisations.index')->with('success','Delete Successfully');
	}



	private function image($realisation,$request,$nbimage){
		if($request->hasFile($nbimage)){
			$imageName = $request->slug.'-'.$nbimage.'.jpg';
			if($realisation->$nbimage) {
				File::delete(public_path("/images/reals/default/".$realisation->$nbimage));
				File::delete(public_path("/images/reals/2000/".$realisation->$nbimage));
				File::delete(public_path("/images/reals/1500/".$realisation->$nbimage));
				File::delete(public_path("/images/reals/800/".$realisation->$nbimage));
			};

			Image::make($request->$nbimage)->resize(2000, null, function ($constraint) {$constraint->aspectRatio();})->encode('jpg',60)->save(public_path()."/images/reals/default/".$imageName);
			Image::make($request->$nbimage)->resize(2000, null, function ($constraint) {$constraint->aspectRatio();})->encode('jpg',60)->save(public_path()."/images/reals/2000/".$imageName);
			Image::make($request->$nbimage)->resize(1500, null, function ($constraint) {$constraint->aspectRatio();})->encode('jpg',60)->save(public_path()."/images/reals/1500/".$imageName);
			Image::make($request->$nbimage)->resize(800, null, function ($constraint) {$constraint->aspectRatio();})->encode('jpg',60)->save(public_path()."/images/reals/800/".$imageName);

			return $imageName;
		}else{
			return ($realisation->$nbimage) ? $realisation->$nbimage : null;
		}
	}

	public function deletephoto(request $request){
		$photo = $request->avantapres;
		$id = $request->supprimer;
		$realisation = Realisation::findOrFail($id);

		if($realisation->$photo) {
			File::delete(public_path("/images/reals/default/".$realisation->$photo));
			File::delete(public_path("/images/reals/2000/".$realisation->$photo));
			File::delete(public_path("/images/reals/1500/".$realisation->$photo));
			File::delete(public_path("/images/reals/800/".$realisation->$photo));
		};

		$realisation->update([
			$photo=>null
		]);

		return true;
	}
}
