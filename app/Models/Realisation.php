<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Realisation  extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'titre',
        'slug',
        'description',
		'avant',
		'apres',
		'category_id',
		'avis_titre',
		'avis_description'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [

	];

	public function category(){
		return $this->belongsTo("App\Models\Category");
	}
}
