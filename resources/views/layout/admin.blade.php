<!doctype html>
<html lang="fr">

<head>
	<title>ADMIN - @yield('title'){{config('app.name')}}</title>
	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<!--     Fonts and icons     -->
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
	<!-- Material Kit CSS -->
	<link href="{{ asset('/css/material-dashboard.min.css') }}" rel="stylesheet" />
	<script src="{{ asset('js/app.js') }}"></script>
	<script src="{{ asset('js/ajax.js') }}"></script>

</head>

<body>
	<div class="wrapper ">
		<div class="sidebar" data-color="purple" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">
			<div class="logo">
				<span class="simple-text logo-normal">
					L'Art de la Pierre
				</span>
			</div>
			<div class="sidebar-wrapper">
				<ul class="nav text-center">
					@if(Auth::check())

					<li class="nav-item">
						<a class="nav-link" href="{{ route('admin-index') }}">Accueil</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="{{ route('realisations.index') }}">Réalisations</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="{{ route('categories.index') }}">Catégories</a>
					</li>
					<li class="nav-item active-pro">
						<form method="POST"action="{{ route('logout') }}">
							@csrf
							<button type="submit"class="nav-link btn btn-link">Déconnexion</button>
						</form>
					</li>
					<li class="nav-item ">
					<a class="btn btn-link" href="{{ route('user') }}">
						{{ __('Changer de Mot de passe') }}
					</a>
					</li>
					@else
					<li class="nav-item">
						<a class="nav-link" href="{{ route('accueil') }}">Revenir au site</a>
					</li>

					<!-- Navbar -->

					@endif

				</ul>
			</div>
		</div>
		<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
			<div class="container-fluid">

				<button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
					<span class="sr-only">Toggle navigation</span>
					<span class="navbar-toggler-icon icon-bar"></span>
					<span class="navbar-toggler-icon icon-bar"></span>
					<span class="navbar-toggler-icon icon-bar"></span>
				</button>

			</div>
		</nav>



		<div class="main-panel">
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							@yield('content')
						</div>
					</div>
				</div>
			</div>
			<footer class="footer">
				<div class="container-fluid">
				</div>
			</footer>
		</div>
	</div>

</body>
</html>
