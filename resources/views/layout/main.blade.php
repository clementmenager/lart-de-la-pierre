<!DOCTYPE html>
<html lang="@lang('layout.title')">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>@yield('title'){{config('app.name')}}</title>
	<link rel="icon" href="{{ asset('images/Logo.png') }}" type="image/png">

	<!-- LARAVEL -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	{{-- CSS --}}
	<link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin=""/>
	<link rel="stylesheet" href="{{ mix('css/app.css') }}">


	{{-- JS --}}
	<script src="https://code.iconify.design/1/1.0.7/iconify.min.js"></script>
	<script src="{{ mix('js/app.js') }}" defer></script>
	{{-- <script src="{{ mix('js/lazy-load.js') }}"></script> --}}

	@yield('includes')
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-DPFGZGSQ2B"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'G-DPFGZGSQ2B');
	</script>
</head>
<body>
	<div id="carouselExampleSlidesOnly" class="carousel slide carousel-fade " data-ride="carousel" data-interval="7000">
		<div class="carousel-inner">
			<div class="caption w-100 d-flex justify-content-center">
				<div class="blur">
					<img class="" src="{{ asset('images/Logoblack.png') }}">
				</div>
			</div>
			<div class="carousel-item active">
				<img class="d-block" src="{{ asset('images/load.gif') }}" data-src="{{ asset('images/default/carousel3.jpg') }}" alt="Creation d'un mur par l Art de la pierre">
			</div>
			<div class="carousel-item">
				<img class="d-block" src="{{ asset('images/load.gif') }}" data-src="{{ asset('images/default/carousel2.jpg') }}" alt="Renovation toit par l Art de la pierre">
			</div>
			<div class="carousel-item">
				<img class="d-block" src="{{ asset('images/load.gif') }}" data-src="{{ asset('images/default/carousel1.jpg') }}" alt="Renovation d'une terrasse par l Art de la pierre">
			</div>
		</div>
	</div>

	<nav class="navbar navbar-expand-lg navbar-light sticky-top">
		<a class="navbar-brand" href="#">
			<img class="" src="{{ asset('images/Logoblack.png') }}">
		</a>

		<button class="navbar-toggler " type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse justify-content-center" id="collapsibleNavbar">
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="nav-link" href="{{ route('accueil') }}">@lang('layout.accueil')</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="{{ route('realisations') }}">@lang('layout.rea')</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="{{ route('taille') }}">@lang('layout.taille')</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="{{ route('maçonnerie') }}">@lang('layout.maçonnerie')</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="{{ route('contact') }}">@lang('layout.cont')</a>
				</li>

				<li class="nav-item">
					<a class="nav-link" href="{{ route('lang') }}" title="@lang('layout.lang.title')">@lang('layout.lang.txt')</a>
				</li>

			</ul>
		</div>
	</nav>
	@yield('content')
	<h2 class="text-center nopadding">@lang('layout.contact')</h2>
	<footer style="background-image: url({{asset('images/2000/footer.jpg')}})">

		<div class="flex">
			<div class="one">
				<div class="contact">
					<div class="w-100 d-flex justify-content-center">
						<img class="" src="{{ asset('images/Logoblack.png') }}">
					</div>
					<div class="text-center">
						<div class="ligne">
							<span class="iconify" data-icon="ant-design:mail-outlined" data-inline="false"></span>

							<a href="mailto:contact@artdelapierre.net">contact@artdelapierre.net</a>
						</div>
						<div class="ligne">
							<span class="iconify" data-icon="bi:phone" data-inline="false"></span>
							<a href="tel:0784024325">07 84 02 43 25</a>
						</div>
					</div>
				</div>
			</div>
			<div class="two">
				<div class="map" id="mapid">
				</div>
			</div>

		</div>
	</footer>
	<div class="copy">
		<p>©{{ now()->year }} Les Petites Mains du Limousin</p>
		<p>Site réalisé avec ❤️ par <a href="https://latoile.dev">LaToile.dev</a></p>
		<p><a href="{{ route('mentions') }}">Mentions Légales</a></p>
	</div>
</body>
</html>
