{!! Form::model($realisation, ['route' => ["realisations.".$action, $realisation->id], 'method' => $action=="store"?"post":"put", 'files' => true])!!}
<a class="btn btn-default" href="{{ route('realisations.index') }}">Annuler</a>
<a class="btn btn-info float-right" href=" {{ route('realisations')."#".$realisation->id }}" target="_bank">Voir le projet</a>
<div class="card">
	@if ($action=="store")
	<div class="card-header card-header-success">
		<h4 class="card-title ">Ajouter</h4>
		<p class="card-category"> Voici les différents paramètres à remplire</p>
		@else
		<div class="card-header card-header-warning">
			<h4 class="card-title ">Modifier</h4>
			<p class="card-category"> Voici les différents paramètres qu'il est possible de modifier</p>
			@endif
		</div>
		<div class="card-body">
			<div class="form-group">
				<p for="titre">Titre</p>
				{!! Form::text('titre', null, $attributes = ['required','class' => 'form-control','id'=>'titre','placeholder'=>'Entrer un titre']) !!}
			</div>
			<div class="form-group">
				<p for="description">Description</p>
				{!! Form::textarea('description', null, $attributes = ['required','class'=>'form-control','id'=>'description','placeholder'=>'Entrer une description ...']) !!}
			</div>

			<div>
				<p for="file">Image-Avant</p>
				<input type="file" name="avant" class="form-group">
				<img @if($realisation->avant != null) src="{{ asset("images/reals/default/".$realisation->avant) }}" @endif style="max-width:250px;margin-top:20px;">
				<button class="btn-suppr" data-name="avant" data-id="{{$realisation->id}}">Supprimer</button>
			</div>
			<div>
				<p for="file">Image-Après</p>
				<input type="file" name="apres" class="form-group">
				<img @if($realisation->apres != null) src="{{ asset("images/reals/default/".$realisation->apres) }}" @endif style="max-width:250px;margin-top:20px;">
				<button class="btn-suppr" data-name="apres" data-id="{{$realisation->id}}">Supprimer</button>
			</div>

			<div class="form-group">
				<p for="category_id">Selectionner la Catégorie</p>
				{!! Form::select('category_id',$categories, null, ['placeholder'=>'Catégorie','id'=>'category_id','required','class'=>'form-control selectpicker']) !!}
			</div>

			<div class="form-group">
				<p for="avis_titre">Titre avis</p>
				{!! Form::text('avis_titre', null, $attributes = ['class'=>'form-control','id'=>'avis_titre','placeholder'=>'Entrer le titre de l\'avis ']) !!}
			</div>
			<div class="form-group">
				<p for="avis_description">Description avis</p>
				{!! Form::textarea('avis_description', null, $attributes = ['class'=>'form-control','id'=>'avis_description','placeholder'=>'Entrer la description de l\'avis ...']) !!}
			</div>

			<div class="form-group text-center">
				@if ($action=="store")
				{!! Form::submit('Enregistrer',['class'=> 'btn btn-success ']) !!}
				@else
				{!! Form::submit('Enregistrer',['class'=> 'btn btn-warning ']) !!}
				@endif
			</div>


			{!! Form::hidden('slug') !!}
			{!! Form::close() !!}
		</div>
	</div>
</div>

<script>

	$("input[type=file]").on("change", function(){
		var file=$(this).get(0).files[0];
		// console.log(file,"2");
		var img = $(this).parent().find('img');
		if(file) {
			var reader = new FileReader();
			reader.onload=function(){
				img.attr("src",reader.result);
			}
			reader.readAsDataURL(file);
		}
	});

	$("input[name='titre']").keyup(function(){
		var value=$(this).val();
		console.log(value);
		var slug= value.toLowerCase().replace(/[^a-z0-9]/g,'-')
		$("input[name='slug']").val(slug);
	})


</script>
