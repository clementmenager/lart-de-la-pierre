{{-- {{ dd($action) }} --}}
{!! Form::model($item, ['route' => [explode('.',\Request::route()->getName())[0].".".$action, $item->id], 'method' => $action=="store"?"post":"put"])!!}
<a class="btn btn-default" href="{{ route(explode('.',\Request::route()->getName())[0].'.index') }}">Annuler</a>
<div class="card">
	@if ($action=="store")
	<div class="card-header card-header-success">
		<h4 class="card-title ">Ajouter</h4>
		<p class="card-category"> Voici les différents paramètres à remplire</p>
		@else
		<div class="card-header card-header-warning">
			<h4 class="card-title ">Modifier</h4>
			<p class="card-category"> Voici les différents paramètres qu'il est possible de modifier</p>
			@endif
		</div>
		<div class="card-body">
			<form>
				<div class="form-group">
					<label for="exampleInputEmail1">Titre</label>
					{!! Form::text('titre', null, $attributes = ['required','class' => 'form-control','placeholder'=>'Entrer un titre']) !!}
				</div>
				<div class="form-group text-center">
					@if ($action=="store")
					{!! Form::submit('Enregistrer',['class'=> 'btn btn-success ']) !!}
					@else
					{!! Form::submit('Enregistrer',['class'=> 'btn btn-warning ']) !!}
					@endif
				</div>
				{!! Form::close() !!}
			</form>
		</div>
	</div>
</div>
