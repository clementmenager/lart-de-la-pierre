@extends('layout.admin')

@section('content')
@if (isset($realisation))
<h1>Ajouter une nouvelle Réalisation</h1>
@include('pages.admin.realisation',["action"=>"store"])
@else
<h1>Ajouter une nouvelle ligne</h1>
@include('pages.admin.form',["action"=>"store"])
@endif
@endsection
