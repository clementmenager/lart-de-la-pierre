@extends('layout.admin')

@section('content')

@if ($message = Session::get('success'))
<div class="alert alert-success alert-block text-center">
	<span>{{ $message }}</span>
</div>
@endif
@if ($message = Session::get('error'))
<div class="alert alert-danger alert-block text-center">
	<span>{{ $message }}</span>
</div>
@endif

<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-8">
			<div class="card">
				<div class="card-header"><h1>Reset mot de passe</h1></div>
				<div class="card-body">
					{{ Form::open(['route' => 'recup']) }}
						<div class="form-group row">
							<label for="ancien" class="col-md-4 col-form-label text-md-right">{{ __('Ancien mot de passe') }}</label>

							<div class="col-md-6">
								{!! Form::password('ancien', null, $attributes = ['required','class'=>'form-control','id'=>'ancien','placeholder'=>'Nouveau mot de passe']) !!}

							</div>
						</div>
						<div class="form-group row">
							<label for="nouveau" class="col-md-4 col-form-label text-md-right">{{ __('Nouveau Mot de passe') }}</label>
							<div class="col-md-6">
								{!! Form::password('nouveau', null, $attributes = ['required','class'=>'form-control','id'=>'nouveau','placeholder'=>'Nouveau mot de passe']) !!}
							</div>
						</div>
						<div class="form-group row">
							<label for="confirmation" class="col-md-4 col-form-label text-md-right">{{ __('Confirmation') }}</label>
							<div class="col-md-6">
								{!! Form::password('confirmation', null, $attributes = ['required','class'=>'form-control','id'=>'confirmation','placeholder'=>'Confirmation']) !!}
							</div>
						</div>
						<div class="form-group row mb-0 justify-content-center">
							{!! Form::submit('Confirmer',['class'=> 'btn btn-success ']) !!}
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
