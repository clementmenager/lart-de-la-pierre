@extends('layout.admin')

@section('content')
@if (isset($realisation))
@include('pages.admin.realisation',["action"=>"update"])
@else
@include('pages.admin.form',["action"=>"update"])
@endif
@endsection
