@extends('layout.admin')

@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">{{ __("Page d'acceuil") }}</div>

				<div class="card-body">
					@if (session('status'))
					<div class="alert alert-success" role="alert">
						{{ session('status') }}
					</div>
					@endif

					{{ __('Vous êtes connectés !') }}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
