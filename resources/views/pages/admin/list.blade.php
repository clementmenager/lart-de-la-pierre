@extends('layout.admin')

@section('content')

@if (session('success'))
<div class="alert alert-success">
	{{ session('success') }}
</div>
@endif

@if (session('error'))
<div class="alert alert-danger">
	{{ session('error') }}
</div>
@endif

<a href="{{ route(explode('.',\Request::route()->getName())[0].".create") }}" class="btn btn-success">Ajouter</a>
<a href="{{ route('realisations') }}" class="btn btn-info float-right" target="_bank">Voir les réalisations</a>
<div class="card">
	<div class="card-header card-header-primary">
		@if (explode('.',\Request::route()->getName())[0] == "realisations")
		<h4 class="card-title ">Réalisations</h4>
		<p class="card-category"> Voici les différentes réalisation</p>
		@elseif (explode('.',\Request::route()->getName())[0] == "categories")
		<h4 class="card-title ">Catégorie</h4>
		<p class="card-category"> Voici les différentes catégories</p>
		@endif
	</div>
	<div class="card-body">
		<div class="table-responsive">
			<table class="table">
				<thead class=" text-primary">
					<th>
						ID
					</th>
					<th>
						Titre
					</th>
					@if(\Request::route()->getName() == "realisations.index" )
					<th>
						Descritpion
					</th>
					<th>
						Avant
					</th>
					<th>
						Après
					</th>
					@endif
					<th>
						Action
					</th>
				</thead>
				<tbody>
					@foreach ($collection as $item)

					<tr>
						<td>
							{{ $item->id }}
						</td>
						<td>
							{{ $item->titre }}
						</td>
						@if(\Request::route()->getName() == "realisations.index" )
						<td>
							{{ substr($item->description,0,200)."..." }}
						</td>
						<td>
							<img @if($item->avant) src="{{ asset("images/reals/default/".$item->avant) }}" @endif style="max-width:250px;margin-top:20px;">
						</td>
						<td>
							<img @if($item->apres) src="{{ asset("images/reals/default/".$item->apres) }}" @endif style="max-width:250px;margin-top:20px;">
						</td>
						@endif
						<td>
							<a href="{{ route(explode('.',\Request::route()->getName())[0].'.edit', $item->id ) }}" class="btn btn-warning">Modifier</a>
							<form action="{{ route(explode('.',\Request::route()->getName())[0].'.destroy', $item->id)}}" method="post">
								@csrf @method('DELETE')
								<button class="btn btn-danger" type="submit" onclick="return confirm('Etes-vous sur de vouloir supprimer')">Delete</button>
							</form>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>


@endsection
