@extends('layout.main')
@section('title',"Maçonnerie | ")
@section('includes')
<link rel="stylesheet" href="{{ mix('css/taillemaconnerie.css') }}">
@endsection
@section('content')
<div class="titre">
	<h1 class="text-center">@lang('maçonnerie.titre')</h1>
</div>
<div class="text-center bg">
	<div class=" d-flex justify-content-center">
		<img src="{{ asset('images/Logoblack.png') }}">
	</div>
	<h2> @lang('maçonnerie.second-titre') </h2>
	@lang('maçonnerie.description')
	<div class="marg">
		<a href="{{ route('realisations') }}" class="btn btn-secondary">@lang('maçonnerie.rea')</a>
	</div>
</div>
{{-- <div class="container"> --}}
	<div class="carre">
		<div>
			<img class="lazy-load" src="{{ asset('images/load.gif') }}" data-src="{{ asset('images/default/sol5.jpg')}}" alt="sol taillé par l art de la pierre">
		</div>
		<div>
			<img class="lazy-load" src="{{ asset('images/load.gif') }}" data-src="{{ asset('images/default/sol2.jpg')}}" alt="sole tailler par l art de la pierre">
		</div>
		<div>
			<img class="lazy-load" src="{{ asset('images/load.gif') }}" data-src="{{ asset('images/default/escalier4.jpg')}}" alt="escalier mis en place par l art de la pierre">
		</div>
		<div>
			<img class="lazy-load" src="{{ asset('images/load.gif') }}" data-src="{{ asset('images/default/garage3.jpg')}}" alt="garage innovant par l Art de la pierre">
		</div>
	</div>
</div>

@endsection
