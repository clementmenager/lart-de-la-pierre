@extends('layout.main')

@section('title',"")

@section('includes')
	<link rel="stylesheet" href="{{ mix('css/index.css') }}">
	<script src="{{ mix('js/parralax.js') }}" defer></script>
@endsection

@section('content')
<main class="index">
	<div class="titre">
		<h1 class="text-center">@lang('index.titre')</h1>
	</div>
	<div class="inner left">
		<div class="all">
			<div>
				<img class="lazy-load" src="{{ asset('images/load.gif') }}" data-src="{{asset('images/default/taille-de-pierre.jpg')}}" alt="Taille de pierre, Traditionnelle, Renovation, Creuse, Tailleur de pierre Creusois">
			</div>
			<div class="txt">
				<h2>@lang('index.titre-taille')</h2>
				<p>
					@lang('index.description-taille')
				</p>
				<a href="{{ route('taille') }}" class="btn btn-primary">@lang('index.plus')</a>
			</div>
		</div>
	</div>
	<div class="inner right">
		<div class="all">
			<div>
				<img class="lazy-load" src="{{ asset('images/load.gif') }}" data-src="{{asset('images/default/maconnerie.jpg')}}" alt="Maconnerie, Maconerie, Travaux, Renovation de terrasse, Façade, Macon Creusois">
			</div>
			<div class="txt">
				<h2>@lang('index.titre-maçonnerie')</h2>
				<p>
					@lang('index.description-maçonnerie')
				</p>
				<a href="{{ route('maçonnerie') }}" class="btn btn-secondary"> @lang('index.plus')</a>
			</div>
		</div>
	</div>
	<section class="justify-content-center ">
		<div class="all">
			<div class="vue">
				<div class="layer">
					<div class="img top">
						<img class="lazy-load" src="{{ asset('images/load.gif') }}" data-src="{{asset('images/800/banc.jpg')}}" alt="banc créé par l art de la pierre" width="400px">
						<img class="lazy-load" src="{{ asset('images/load.gif') }}" data-src="{{asset('images/800/img2.jpg')}}" alt="mur en construction par l art de la pierre" width="350px">
						<img class="lazy-load" src="{{ asset('images/load.gif') }}" data-src="{{asset('images/800/garage3.jpg')}}" alt="garage innovant par l art de la pierre" width="250px">
						<img class="lazy-load" src="{{ asset('images/load.gif') }}" data-src="{{asset('images/800/sol4.jpg')}}" alt="sol taillé par l art de la pierre" width="200px">
					</div>
					<div class="img">
						<img class="lazy-load" src="{{ asset('images/load.gif') }}" data-src="{{asset('images/800/img3.jpg')}}" alt="mur en construction l art de la pierre" width="200px">
						<img class="lazy-load" src="{{ asset('images/load.gif') }}" data-src="{{asset('images/800/projet1.jpg')}}" alt="rennovation d'une terrasse par l art de la pierre" width="350px">
						<img class="lazy-load" src="{{ asset('images/load.gif') }}" data-src="{{asset('images/800/sol.jpg')}}" alt="sol tailler par l art de la pierre" width="250px">
						<img class="lazy-load" src="{{ asset('images/load.gif') }}" data-src="{{asset('images/800/toit2.jpg')}}" alt="toiture renovée par l art de la pierre" width="400px">
					</div>
					<div class="img bot">
						<img class="lazy-load" src="{{ asset('images/load.gif') }}" data-src="{{asset('images/800/encours2.jpg')}}" alt="travaille en cours de l art de la pierre" width="400px">
						<img class="lazy-load" src="{{ asset('images/load.gif') }}" data-src="{{asset('images/800/escalier4.jpg')}}" alt="escalier fait par l art de la pierre" width="350px">
						<img class="lazy-load" src="{{ asset('images/load.gif') }}" data-src="{{asset('images/800/img1.jpg')}}" alt="mur en construction de l art de la pierre" width="250px">
						<img class="lazy-load" src="{{ asset('images/load.gif') }}" data-src="{{asset('images/800/roche5.jpg')}}" alt="roché de l art de la pierre" width="200px">
					</div>
				</div>
				<div class="logo-txt">
					<a href="{{ route('realisations') }}" class="btn">@lang('index.voir-projet')</a>
				</div>
			</div>
		</div>
	</section>

	@if ($avis >= 3)
	<div class="slider">
		<div class="slide-track">
			@for ($i = 0; $i < 2; $i++)
				@foreach ($realisations as $realisation)
					@if ($realisation->avis_titre)
						<div class="slide">
							<div class="style">
								<h2>{{ $realisation->avis_titre }}</h2>
								<p>{{ $realisation->avis_description }}</p>
								<a href=" {{ route('realisations')."#".$realisation->id }}">@lang('index.avisbtn')</a>
							</div>
						</div>
					@endif
				@endforeach
			@endfor
		</div>
	</div>
	@endif
</main>

<style>
	@keyframes scroll {
		0% {

			transform: translateX(0);
		}
		100% {

			transform: translateX(calc(-33vw * {{ $avis }}));
		}
	}
	.slider .slide-track{
		width: calc(33vw*  {{ $avis }}*2);
	}
	.slider .slide{
		width: 33vw;
	}
</style>
@endsection
