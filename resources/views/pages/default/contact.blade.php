@extends('layout.main')
@section('title',"Contact | ")
@section('includes')
<link rel="stylesheet" href="{{ mix('css/contact.css') }}">
<script src="{{ mix('js/captcha.js') }}" defer></script>
@endsection
@section('content')

<div class="titre">
	<h1 class="text-center">@lang('contact.titre')</h1>
</div>
<div class="container">

	@if ($message = Session::get('success'))
	<div class="alert alert-success alert-block text-center">
		<span>{{ $message }}</span>
	</div>
	@endif

	@if($errors->any())
		{!! implode('', $errors->all('<div class="alert alert-danger alert-block text-center"><span>:message</span></div>')) !!}
	@endif



		{{ Form::open(['route' => 'sendmail']) }}
		<input type="hidden" id="g-recaptcha-response" name="g-recaptcha-response" required>
		<div class="row">
			<div class="form-group col-sm-6">
				<label for="nom">@lang('contact.nom')</label>
				{!! Form::text('nom', null, $attributes = ['required','class' => 'form-control'.($errors->has('nom') ? ' is-invalid' : ''),'id'=>'nom','placeholder'=>__('contact.nom')]) !!}
			</div>
			<div class="form-group col-sm-6">
				<label for="prenom">@lang('contact.prenom')</label>
				{!! Form::text('prenom', null, $attributes = ['required','class' => 'form-control'.($errors->has('prenom') ? ' is-invalid' : ''),'id'=>'prenom','placeholder'=>__('contact.prenom')]) !!}
			</div>

			<div class="form-group col-sm-6">
				<label for="mail">@lang('contact.mail')</label>
				{!! Form::email('email', null, $attributes = ['required','class' => 'form-control'.($errors->has('email') ? ' is-invalid' : '') ,'id'=>'mail','placeholder'=>__('contact.placeholder-mail')]) !!}
			</div>
			<div class="form-group col-sm-6">
				<label for="tel">@lang('contact.tel')</label>
				{!! Form::text('tel', null, $attributes = ['required','class' => 'form-control'.($errors->has('tel') ? ' is-invalid' : ''),'id'=>'tel','placeholder'=>__('contact.placeholder-tel')]) !!}
			</div>
		</div>
		<div class="row">
			<div class="form-group col">
				<label for="txt">@lang('contact.message')</label>
				{!! Form::textarea('message', null, $attributes = ['class'=>'form-control'.($errors->has('message') ? ' is-invalid' : ''),'id'=>'message','placeholder'=>__('contact.placeholder-message')]) !!}

			</div>
		</div>
		{!! Form::submit(__('contact.envoyer'),['class'=> 'btn btn-primary form-group']) !!}
		{!! Form::close() !!}
	</div>



	@endsection
