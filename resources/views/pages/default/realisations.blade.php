@extends('layout.main')
@section('title',"Réalisations | ")
@section('includes')
<link rel="stylesheet" href="{{ mix('css/realisations.css') }}">
<script src="{{ mix('js/realisations.js') }}" defer></script>
@endsection
@section('content')

<div class="titre">
	<h1 class="text-center">Nos Réalisations</h1>
</div>
<div class="select">
	<span class="btn btn-info" data-id="all">Tous</span>
	<span class="btn btn-info" data-id="Taille de Pierre">Taille de pierre</span>
	<span class="btn btn-info" data-id="Maçonnerie">Maçonnerie</span>
	<span class="btn btn-info" data-id="Rénovation">Rénovation</span>
</div>

@foreach ($realisations as $realisation)
<div class="realisation" data-id="{{ $realisation->category->titre}}" id="{{ $realisation->id }}">
	<div class="slide">
		@if(!$realisation->apres && !$realisation->avant)
		<div class="seul">
			<img class="lazy-load" src="{{ asset('images/load.gif') }}" data-src="{{ asset("images/default/chat.jpg") }}">
		</div>
		@else
			@if(!$realisation->apres || !$realisation->avant)
			<div class="seul">
				<img class="lazy-load" src="{{ asset('images/load.gif') }}" data-src="{{ asset("images/reals/default/".(($realisation->apres) ? $realisation->apres : $realisation->avant)) }}">
			</div>
			@else
				<div class="avant">
					<img class="lazy-load" src="{{ asset('images/load.gif') }}" data-src="{{ asset('images/reals/default/'.$realisation->apres) }}">
				</div>
				<div class="apres">
					<img class="lazy-load" src="{{ asset('images/load.gif') }}" data-src="{{ asset('images/reals/default/'.$realisation->avant) }}">
				</div>

				<input type="range" min="0" max="100" value="50">
				<div class="grab">
					<span class="iconify" data-icon="bx:bxs-left-arrow" data-inline="false"></span>
					<span class="iconify" data-icon="bx:bxs-right-arrow" data-inline="false"></span>
				</div>
			@endif
		@endif
	</div>
	<div class="txt">
		<h3>{{ $realisation->titre }}</h3>
		<p>{{ $realisation->description }}</p>

	</div>
</div>
@endforeach

@endsection
