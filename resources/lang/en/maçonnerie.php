<?php

return [

    'titre' => "Masonry",

	'second-titre' => "Mason since 1994",

	'description' => "<b>L'art de la pierre</b> works just as well in <b>masonry</b>, because for us it is important to <b>create</b> and <b>renovate</b>, that's why we offer you different types of <b>labor</b>. We work in <b>Creuse</b> and we ensure the smooth running of the <b>site</b> so that it is <b>secure</b> and finished in record time. We specialize in masonry, etc. But we can also work well in the <b>construction</b> of a building from A to Z.<br>
	Being in constant evolution since 1994, we help you in your <b>renovation choices</b> and <b>choice of material</b> to have a <b>best value for money</b> in any condition.",

	'rea'=>"Our achievements",

];
