<?php

return [

	"titre"=>'Contact',

	'nom'=>"Last Name",
	'prenom'=>"First Name",
	'mail'=>"Mail",
	'placeholder-mail'=>"E-mail",
	'tel'=>"Phone",
	'placeholder-tel'=>"Phone Number",
	'message'=>"Message",
	'placeholder-message'=>"Your message",
	'envoyer'=>"Send",

];
