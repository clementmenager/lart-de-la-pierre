<?php

return [

	'titre'=>"Qui sommes-nous ?",

    'titre-taille' => 'Taille de pierre',
    'description-taille' => "<b>L'art de la pierre</b> est une <b>entreprise creusoise</b>, travaillant la pierre de façon <b>traditionnelle</b>, ce qui est l'une de nos plus <b>grandes valeurs</b>. Nous intervenons dans toute la <b>Creuse</b>. Nous assurons le <b>bon déroulement du chantier</b> pour que chaque chantier soit <b>sécurisé</b> et fini dans <b>un temps record</b>. Nous nous sommes spécialisés dans la <b>rénovation</b> ...",

    'titre-maçonnerie' => 'Maçonnerie',
    'description-maçonnerie' => "<b>L'art de la pierre</b> travaille tout aussi bien dans la <b>maçonnerie</b>, car pour nous, il est important de <b>créer</b> et de <b>rénover</b>, c'est pourquoi nous vous proposons différents types de <b>main d'œuvre</b>. Nous intervenons en <b>Creuse</b> et nous assurons le bon déroulement du <b>chantier</b> pour qu'il soit <b>sécurisé</b> et fini dans un temps record. Nous nous sommes spécialisés ...",

	'voir-projet'=>"Voir tous les projets",

	'plus'=>"En savoir plus",

	'avisbtn'=>"Voir le projet",

];
