<?php

return [

	"titre"=>'Contact',

	'nom'=>"Nom",
	'prenom'=>"Prènom",
	'mail'=>"mail",
	'placeholder-mail'=>"E-mail",
	'tel'=>"Téléphone",
	'placeholder-tel'=>"Numéro de téléphone",
	'message'=>"Message",
	'placeholder-message'=>"Votre message",
	'envoyer'=>"Envoyer",

];
