<?php

return [

    'titre' => "Taille de Pierre",

	'second-titre' => "Taille de Pierre depuis 1994",

	'description' => "<b>L'art de la pierre</b> est une <b>entreprise creusoise</b>, travaillant la pierre de façon <b>traditionnelle</b>, ce qui est l'une de nos plus <b>grandes valeurs</b>. Nous intervenons dans toute la <b>Creuse</b>. Nous assurons le <b>bon déroulement du chantier</b> pour que chaque chantier soit <b>sécurisé</b> et fini dans <b>un temps record</b>. Nous nous sommes spécialisés dans la <b>rénovation</b> de bâtiment intérieur comme d'extérieur, mais nous pouvons tout aussi bien travailler sur les terrasses en plein air et autres...<br>
	Étant en constante évolution depuis 1994, nous vous aidons dans le <b>choix du matériel approprié</b> pour avoir un <b>meilleur rapport qualité prix</b> dans n'importe quelle condition.",

	'rea'=>"Nos réalisations",
];
