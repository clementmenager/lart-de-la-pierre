<?php

return [

    'titre' => 'Maçonnerie',

	'second-titre' => "Maçon depuis 1994",

	'description' => "<b>L'art de la pierre</b> travaille tout aussi bien dans la <b>maçonnerie</b>, car pour nous, il est important de <b>créer</b> et de <b>rénover</b>, c'est pourquoi nous vous proposons différents types de <b>main d'œuvre</b>. Nous intervenons en <b>Creuse</b> et nous assurons le bon déroulement du <b>chantier</b> pour qu'il soit <b>sécurisé</b> et fini dans un temps record. Nous nous sommes spécialisés dans le murage, etc. Mais nous pouvons aussi bien travailler dans la <b>construction</b> d'un bâtiment de A à Z.<br>
	Étant en constante évolution depuis 1994, nous vous aidons dans vos <b>choix de rénovation</b> et du <b>choix du matériel</b> pour avoir un <b>meilleur rapport qualité prix</b> dans n'importe quelle condition.",


	'rea'=>"Nos réalisations",
];
