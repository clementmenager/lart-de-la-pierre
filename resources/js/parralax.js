// PARRALAX
$('.vue').on("mousemove", function(event){
	const ratioX = 20/$(this).width();
	const ratioY = 20/$(this).height();
	const x = (event.pageX - $(this).offset().left)*ratioX-10;
	const y = (event.pageY - $(this).offset().top)*ratioY-10;
	$('.layer').css({"transition":"0s","transform":"translate("+x*-1+"%,"+y*-1+"%)"})
});

$('.vue').on("mouseleave", function(e){
	$('.layer').css({"transform":"translate(0,0)"})
	$('.layer').css({"transition":"500ms ease-in-out"})

});
