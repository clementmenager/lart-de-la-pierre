//LEAFLET
if (document.querySelector('#mapid') !== null) {

$.getScript('https://unpkg.com/leaflet@1.7.1/dist/leaflet.js', () => {


var mymap = L.map('mapid',{
	attributionControl:false,
}).setView([45.955269, 2.168405], 14);
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
maxZoom: 18,
id: 'mapbox/streets-v11',
tileSize: 512,
zoomOffset: -1,
accessToken: 'pk.eyJ1IjoiZnVyc29uaXRlIiwiYSI6ImNqcGJnZnRscjBlbW4za3BocTl3OXo0NmIifQ.XRMsCJ51pNuqZTBGZJBEnA',
zoomControl: false,
}).addTo(mymap);
L.control.attribution({ position: 'bottomleft' }).addTo(mymap);
var marker = L.marker([45.955269, 2.168405]).addTo(mymap);
mymap.zoomControl.setPosition('topright');
})
}
