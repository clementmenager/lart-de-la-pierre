$(function(){

	$(".btn-suppr").on('click', function(event){
		event.preventDefault();

		var supprimer = $(this).data("id");
		var avantapres= $(this).data("name");
		var _token = $('input[name="_token"]').val();
		var mythis = $(this);
		$.ajax({
			url : "/admin/delete-photo",
			data : {
				supprimer,
				avantapres,
				_token
			},
			method : "POST",
		}).done(function(data) {
			(data) ? mythis.parent().find('img').hide() : alert("Problème de suppression")
		});
	})
});
