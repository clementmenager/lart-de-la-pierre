window.$ = window.jQuery = require('jquery');
require('bootstrap');

//LEAFLET
require('./leaflet')

// //SRCSET AUTO
$('img').each(function(){
	src = ($(this).data('src')) ? $(this).data('src') : $(this).attr('src');
	src800 = src.replace('default','800');
	src1500 = src.replace('default','1500');
	src2000 = src.replace('default','2000');
	srcset = src800 + " 800w, " + src1500 + " 1500w, " + src2000 + " 2000w";
	$(this).attr('srcset',srcset);
})

//active navbar
var url = (location.pathname=="/") ? "" : location.pathname;
$('.navbar-nav a[href="' + location.origin + url + '"]').addClass('active');
