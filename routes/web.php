<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\RealisationController;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\AdminController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/',[PublicController::class,'index'])->name('accueil');
Route::get('realisations',[PublicController::class,'realisations'])->name('realisations');
Route::view('maconnerie','pages.default.maçonnerie')->name('maçonnerie');
Route::view('taille-de-pierre','pages.default.taille')->name('taille');
Route::view('contactez-moi','pages.default.contact')->name('contact');
Route::view('mentions','pages.default.mentions')->name('mentions');
Route::post('sendmail',[PublicController::class,'sendmail'])->name('sendmail');

Route::prefix('admin')->group(function () {
	Auth::routes();
	Route::middleware('auth')->group(function () {
		Route::view('/','pages.admin.index')->name('admin-index');
		Route::resource('categories', CategoriesController::class);
		Route::resource('realisations', RealisationController::class);
		Route::get('user',[AdminController::class,'mdp'])->name('user');
		Route::post('recup',[AdminController::class,'recup'])->name('recup');
		Route::post('delete-photo',[RealisationController::class,'deletephoto']);
	});
});

Route::get('/lang',[PublicController::class,'lang'])->name('lang');
