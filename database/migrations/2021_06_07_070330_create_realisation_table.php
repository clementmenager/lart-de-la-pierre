<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRealisationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
			$table->string('titre');
            $table->timestamps();
        });
        Schema::create('realisations', function (Blueprint $table) {
            $table->id();
			$table->foreignId('category_id');
            $table->text('titre');
            $table->text('avis_titre');
            $table->text('slug');
            $table->longText('description');
            $table->longText('avis_description');
            $table->string('avant')->nullable();
            $table->string('apres')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
        Schema::dropIfExists('realisations');
    }
}

